package main

import (
	"fmt"
	"math/rand"
	"os"
)

func main() {

	localFile, err := createLocalFile()

	if err != nil {
		fmt.Print("Error main", err)
	}

	defer localFile.Close()

	writeInLocalFile(localFile)
}

func writeInLocalFile(file *os.File) {

	numbers := getRandomNumbers(defineSize())

	var sumBytes int

	for _, num := range numbers {
		var n int
		n, err := fmt.Fprintf(file, "%d\n", num)
		if err != nil {
			fmt.Print("Error writing in txt file", err)
			return
		}
		sumBytes = sumBytes + n
	}

	fmt.Print("total bytes written", sumBytes)
}

func getRandomNumbers(limit int) []int {

	var arrayOfRandomNumbers []int

	for number := 0; number < limit; number++ {

		randomNumber := rand.Intn(100)

		arrayOfRandomNumbers = append(arrayOfRandomNumbers, randomNumber)

	}
	return arrayOfRandomNumbers
}

func defineSize() (size int) {
	var mySize int
	fmt.Print("Ingrese el tamaño del array: ")
	fmt.Scanln(&mySize)
	return mySize
}

func createLocalFile() (*os.File, error) {
	archivoLocal, err := os.Create("example.txt")

	if err != nil {
		fmt.Print("Error creating local txt file", err)
		return nil, err
	}

	return archivoLocal, nil
}
